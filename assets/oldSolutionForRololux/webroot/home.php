<!DOCTYPE html>
<html lang="sr-RS">
<head>
    <meta charset="utf-8">
    <title>RoloLux</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>

<?php
    include 'navigation.php';
?>

<section>
    <div>
        <!-- slajder -->
    </div>
</section>

<section>
    <div>
        <header>
            <h1>O nama</h1>
            <hr>
            <p>Firma Rololux ns je osnovana 1950. godine kao samostalna zanatska radnja za proizvodnju i odrzavanje roletni. Firma je pocela sa radom u Novom Sadu, gde se uspesno i odrzala vec tri generacije. Danas smo, zahvaljujući uspešnim poslovanjem postali ozbiljno privatno preduzeće, koje se bavi proizvodnjom i ugradnjom proizvoda vezanih za enterijer i eksterijer vašeg doma. Vremenom smo došli do zavidnog nivoa proizvodnje korišćenjem savremenih tehnologija, modernizacijom opreme i alata u proizvodnji i praćenjem evropskih i svetskih trendova. Sve to je dovelo do povećanja obima proizvodnje a time i povećanja kvaliteta i proširenja palete proizvoda. Mozemo vam se pohvaliti kvalitetom, strucnim i profesionalnim osobljem, raznovrsnošću programa za opremanje kuća, stanova, poslovnoih prostora i sl...Većina naših proizvoda imaju mogućnostu ugradnje elektro motora i daljinskog upravljanja.</p>
        </header>
    </div>
</section>

<section>
    <div>
        <div>
            <h2>Naša ponuda</h2>
            <hr>
            <div>
                <div>
                    <a href="#"><img src="img/roletne/rol1.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Roletne</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/tende/tenda.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Tende</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/venecijaneri/ven1.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Venecijaneri</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/rolozavese/rol.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Rolo zavese</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/trakastezavese/trazavese.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Trakaste zavese</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/komarnici/komarnici.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Komarnici</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/diht/dihtovanje.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Dihtovanje</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/servis/servis.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Servis</p>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="#"><img src="img/adaptacija/adap.jpg" alt=""/></a>
                    <div>
                        <div>
                            <p>Adaptacija prostora</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div>
        <h3>Kontakt</h3>
        <hr>
        <article>
            <img src="img/home.png" alt=""/>
            <p>Subotička 67 21 000 Novi Sad, Srbija</p>
            <img src="img/phone.png" alt=""/>
            <p>fax 021/ 2466-467 mob 063/8-063-531 mob 064/11-59-532</p>
            <img src="img/email.png" alt=""/>
            <p>rololuxns@yahoo.com</p>
        </article>
        <article>
            <h4>Kako nas pronaći</h4>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2809.348516435344!2d19.8143808095757!3d45.240744796632924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b102fbcfe803f%3A0xebe83980b39f34ad!2z0KHRg9Cx0L7RgtC40YfQutCwIDY3LCDQndC-0LLQuCDQodCw0LQgMjExMjQ!5e0!3m2!1ssr!2srs!4v1498039268694" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </article>
    </div>
</section>

<?php
    include 'footer.php';
?>
</body>
</html>
