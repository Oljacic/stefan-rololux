<header>
    <a href="/"><img src="img/logo.png"></a> <!-- logo -->
    <nav>
        <ul>
            <li><a href="#">O nama</a></li>
            <li><a href="#">Naša ponuda</a>
                <ul>
                    <li><a href="roletne">Roletne</a></li>
                    <li><a href="#">Tende</a></li>
                    <li><a href="#">Venecijaneri</a></li>
                    <li><a href="#">Rolo zavese</a></li>
                    <li><a href="#">Trakaste zavese</a></li>
                    <li><a href="#">Komarnici</a></li>
                    <li><a href="#">Dihtovanje</a></li>
                    <li><a href="#">Servis</a></li>
                    <li><a href="#">Adaptacija prostora</a></li>
                </ul>
            </li>
            <li><a href="#">Kontaktirajte nas</a></li>
        </ul>
    </nav>
</header>
