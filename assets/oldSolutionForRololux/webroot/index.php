<?php

$uri = $_SERVER['REQUEST_URI'];

switch ($uri)
{
    case '/':
        include 'home.php';
        break;
    case '/roletne';
        include 'roletne.php';
        break;
    default:
        http_response_code(404);
}

?>