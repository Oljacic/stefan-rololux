<!DOCTYPE html>
<html lang="en">

<?php include 'head.php';?>

<body>
<header>
    <?php include 'navigation.php'; ?>
</header>

<main>
    <section class="container">
        <div id="carouselControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselControls" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="img-box carousel-item active justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_servis1"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_servis2"></div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <section class="container">
        <div class="row">
            <div class="col-lg-8 section-text">
                <h2>Servis</h2>
                <p>Vršimo sve vrste popravki i održavanja cele palete naših proizvoda. Za sve informacije, osećajte se slobodnim da nam se obratite na neke od naših brojeva telefona.</p>
            </div>
            <?php include 'aside_najtrazenije.php';?>
        </div>
    </section>

</main>

<?php include 'footer.php'; ?>

</body>
</html>