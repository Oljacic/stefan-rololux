<aside class="col-lg-4">
    <div class="card img-fluid">
        <h3 class="card-title">Najtraženije</h3>
        <img class="card-img-top" src="img/aside/najtrazenije.jpg" alt="Eoli Tende">
        <div class="card-block">
            <h4>Eoli Tende</h4>
            <p>Boravak na otvorenom čine lepšim i ugodnijim <a class="btn btn-info btn-sm" href="#">Više ></a></p>
        </div>
    </div>
</aside>