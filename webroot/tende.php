<!DOCTYPE html>
<html lang="en">

<?php include 'head.php';?>

<body>
<header>
    <?php include 'navigation.php'; ?>
</header>

<main>
    <section class="container">
        <div id="carouselControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselControls" data-slide-to="1"></li>
                <li data-target="#carouselControls" data-slide-to="2"></li>
                <li data-target="#carouselControls" data-slide-to="3"></li>
                <li data-target="#carouselControls" data-slide-to="4"></li>
                <li data-target="#carouselControls" data-slide-to="5"></li>
                <li data-target="#carouselControls" data-slide-to="6"></li>
                <li data-target="#carouselControls" data-slide-to="7"></li>
                <li data-target="#carouselControls" data-slide-to="8"></li>
                <li data-target="#carouselControls" data-slide-to="9"></li>
                <li data-target="#carouselControls" data-slide-to="10"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="img-box carousel-item active justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende1"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende2"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende3"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende4"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende5"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende6"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende7"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende8"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende9"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende10"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_tende11"></div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <section class="container">
        <div class="row">
            <div class="col-lg-8 section-text">
                <h2>Tende</h2>
                <p>Tende su potreba, ali i ukras vašeg doma ili poslovnog prostora. Čine zaštitu od prejakog sunca ili svetla, a veličinom i oblikom se mogu prilagoditi svakom zahtevu. Mi vam nudimo celokupan izbor tendi:</p>
                <ul>
                    <li>Ravne</li>
                    <li>Polukružne</li>
                    <li>Kupolaste</li>
                    <li>Samostojeće</li>
                    <li>Fiksne</li>
                    <li>Sa ručnim namotavanjem</li>
                    <li>Sa električnim namotavanjem</li>
                </ul>
                <p>Cena zavisi od veličine, vrste i načina namotavanja, materijala, a:</p>
                <ul>
                    <li>Kvalitet, sigurnost i izuzetan estetski utisak može se postići ugradnjom naših tendi</li>
                    <li>Raznovrsnim izborom boja i dezena platna omogućićemo najprikladnije rešenje za vaš ambijent.</li>
                    <li>Svi tipovi tendi podjednako dobro i kvalitetno štite, kako od sunca, tako i od kiše.</li>
                </ul>
                <p>Boravak na otvorenom čine lepšim i ugodnijim.</p>
            </div>

            <?php include 'aside_najtrazenije.php';?>

        </div>
    </section>
</main>

<?php include 'footer.php'; ?>

</body>
</html>