<!doctype html>
<html lang="en">

<?php include 'head.php';?>

<body>

<?php include 'navigation.php';?>

<div class="error-container">
    <div class="page-error">
        <h2>Page not found</h2>
        <p>Sorry the requested page could not be found on our site. We are constantly adding new gifts, so chances are we've got something even better! You can browse some of our most popular categories on our<a href="/"> site map.</a></p>
    </div>
    <div class="page-error-picture">
        <img src="/img/404.jpg" alt="">
    </div>
    <div class="page-error-p">
        <p>If you need help, please call us on 555-333 Monday-Friday 09.00-17.00 GMT or email us at <a href="#">mail@mail.com</a></p>
    </div>
</div>

</body>
</html>
