<nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse container">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/"><span class="rololux">Rololux</span><span class="ns">Ns</span></a>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/#o-nama">O nama<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="/#ponuda" role="button" aria-haspopup="true" aria-expanded="false">Naša ponuda</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="/roletne">Roletne</a></li>
                    <li><a class="dropdown-item" href="/tende">Tende</a></li>
                    <li><a class="dropdown-item" href="/venecijaneri">Venecijaneri</a></li>
                    <li><a class="dropdown-item" href="/zavese-rolo">Rolo zavese</a></li>
                    <li><a class="dropdown-item" href="/zavese-trakaste">Trakaste zavese</a></li>
                    <li><a class="dropdown-item" href="/komarnici">Komarnici</a></li>
                    <li><a class="dropdown-item" href="/dihtovanje">Dihtovanje</a></li>
                    <li><a class="dropdown-item" href="/servis">Servis</a></li>
                    <li><a class="dropdown-item" href="/adaptacija">Adaptacija prostora</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/#kontakt">Kontaktirajte nas</a>
            </li>
        </ul>
    </div>
</nav>
