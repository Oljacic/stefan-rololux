<!DOCTYPE html>
<html lang="sr-RS">

<?php include 'head.php';?>

<body>
<header>
    <?php include 'navigation.php'; ?>
</header>

<main>
    <section class="container">
        <div id="carouselControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselControls" data-slide-to="1"></li>
                <li data-target="#carouselControls" data-slide-to="2"></li>
                <li data-target="#carouselControls" data-slide-to="3"></li>
                <li data-target="#carouselControls" data-slide-to="4"></li>
                <li data-target="#carouselControls" data-slide-to="5"></li>
                <li data-target="#carouselControls" data-slide-to="6"></li>
                <li data-target="#carouselControls" data-slide-to="7"></li>
                <li data-target="#carouselControls" data-slide-to="8"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="img-box carousel-item active justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne1"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne2"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne3"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne4"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne5"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne6"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne7"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne8"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_roletne9"></div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-lg-8 section-text">
                <h2>Aluminijumske i plastične PVC roletne</h2>
                <p>Roletne su jedan od najstarijih i najrasprostranjenijih zasenjivača prostora Ugrađuju se na prozore ili vrata, sa spoljašnje strane samog otvora. Roletne služe i kao zaštita same stolarije na koju se ugrađuju od vremenskih promena i nepogoda(sunce, kiša, grad, sneg..). Mogu biti plastične ili aluminijumske.I jedne i druge klize gore-dole po vođicama koje mogu biti fiksirane ili na izbacivanje Kod fiksiranih vođica roletna ima mogućnost klizanja smo gore-dole. Za razliku od ovih, vođice na izbacivanje poseduju tzv. makaze koje omogućavaju izbacivanje donjeg dela roletne ka spolja.</p>
                <ul>
                    <li>Kod aluminijumskih roletni vođice takođe mogu biti od aluminijuma i takav tip roletne se zove "total aluminijum". Ovaj tip roletni ima i giksirane vođice i mogućnosti izbacivanja ka spolja. Vođice se obično plastificiraju u boju same roletne.</li>
                    <li>Plastične roletne su jeftinije od aluminijumskih roletni ali se ne preporučuju za velike površine otvora koji se nalaze na sunčanim stranama objekata u koji se roletne ugrađjuj. Za razliku od plastičnih roletni, aluminijumske roletne su dužeg veka trajanja.</li>
                    <li>Aluminijumske roletne se sastoje od aluminijumskih letvica šitine 3.9 cm. Svaka letvica je ispunjena poliureanom što predstavlja odličnu, kako zvučnu, tako i toplotnu izolaciju. Zastor za aluminijumske roletne postoji u nekoliko boja:
                    <ul>
                        <li class="color">Bela</li>
                        <li class="color">Braon</li>
                        <li class="color">Siva</li>
                        <li class="color">Mahagoni</li>
                        <li class="color">Drvo</li>
                    </ul>
                    </li>
                </ul>
            </div>

            <aside class="col-lg-4">
                <div class="card img-fluid">
                    <h3 class="card-title">Izdvajamo</h3>
                    <img class="card-img-top" src="img/aside/izdvajamo.jpg" alt="Screen Rolo Zavese">
                    <div class="card-block">
                        <h4>Screen Rolo Zavese</h4>
                        <p>Boravak u vašem stanu čine lepšim i ugodnijim  <a class="btn btn-info btn-sm" href="#">Više ></a></p>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</main>

<?php include 'footer.php'; ?>

</body>
</html>