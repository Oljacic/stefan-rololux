<!DOCTYPE html>
<html lang="sr-RS">

<?php include 'head.php';?>

<body>

<header>
<?php include 'navigation.php'; ?>
</header>

<main>
    <section class="container">
        <div id="carouselControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselControls" data-slide-to="1"></li>
                <li data-target="#carouselControls" data-slide-to="2"></li>
                <li data-target="#carouselControls" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="img-box carousel-item active justify-content-center">
                        <div class="d-block img-fluid img-responsive img-center slider" id="slider"></div>
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Izrada, popravka i održavanje aluminijumskih i pvc roletni, tendi, venecijanera, trakastih zavesa ... </h3>
                    </div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                        <div class="d-block img-fluid img-responsive img-center" id="slider1"></div>
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Rolo komarnika, plisiranih zavesa, harmonika vrata, dihtovanje prozora i vrata aluminijumskim lajsnama</h3>
                    </div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                        <div class="d-block img-fluid img-responsive img-center" id="slider2"></div>
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Odlična toplotna i zvučna izolacija!</h3>
                    </div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                        <div class="d-block img-fluid img-responsive img-center" id="slider3"></div>
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Brzo i trajno!</h3>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <section id="o-nama" class="album container">
        <h1 class="album_h2">O nama</h1>
        <hr>
        <p class="text_info">Firma Rololux ns je osnovana 1950. godine kao samostalna zanatska radnja za proizvodnju i odrzavanje roletni. Firma je pocela sa radom u Novom Sadu, gde se uspesno i odrzala vec tri generacije. Danas smo, zahvaljujući uspešnim poslovanjem postali ozbiljno privatno preduzeće, koje se bavi proizvodnjom i ugradnjom proizvoda vezanih za enterijer i eksterijer vašeg doma. Vremenom smo došli do zavidnog nivoa proizvodnje korišćenjem savremenih tehnologija, modernizacijom opreme i alata u proizvodnji i praćenjem evropskih i svetskih trendova. Sve to je dovelo do povećanja obima proizvodnje a time i povećanja kvaliteta i proširenja palete proizvoda. Mozemo vam se pohvaliti kvalitetom, strucnim i profesionalnim osobljem, raznovrsnošću programa za opremanje kuća, stanova, poslovnoih prostora i sl...Većina naših proizvoda imaju mogućnostu ugradnje elektro motora i daljinskog upravljanja.</p>
    </section>

    <section id="ponuda">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="album_h2">Naša ponuda</h3>
                    <hr>
                </div>
                <div class="col-lg-4">
                    <a href="/roletne">
                        <img class="img-thumbnail img-fluid" src="img/roletne/rol1.jpg" alt="Roletne"/>
                        <p class="thumb">Roletne</p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="/tende">
                        <img class="img-thumbnail img-fluid" src="img/tende/tenda.jpg" alt="Tende"/>
                        <p class="thumb">Tende</p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="/venecijaneri">
                        <img class="img-thumbnail img-fluid" src="img/venecijaneri/ven1.jpg" alt="Vencijaneri"/>
                        <p class="thumb">Venecijaneri</p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="/zavese-rolo">
                        <img class="img-thumbnail img-fluid" src="img/rolozavese/rol.jpg" alt="Rolo zavese"/>
                        <p class="thumb">Rolo zavese</p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="/zavese-trakaste">
                        <img class="img-thumbnail img-fluid" src="img/trakastezavese/trazavese.jpg" alt="Trakaste zavese"/>
                        <p class="thumb">Trakaste zavese</p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="/komarnici">
                        <img class="img-thumbnail img-fluid" src="img/komarnici/komarnici.jpg" alt="Komarnici"/>
                        <p class="thumb">Komarnici</p>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <a href="/dihtovanje">
                        <img class="img-thumbnail img-fluid" src="img/diht/dihtovanje.jpg" alt="Dihtovanje"/>
                        <p class="thumb">Dihtovanje</p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="/servis">
                        <img class="img-thumbnail img-fluid" src="img/servis/servis.jpg" alt="Servis"/>
                        <p class="thumb">Servis</p>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="/adaptacija">
                        <img class="img-thumbnail img-fluid" src="img/adaptacija/adap.jpg" alt="Adaptacija"/>
                        <p class="thumb">Adaptacija prostora</p>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <section id="kontakt" class="container contact-container">
        <h3 class="album_h2">Kontakt</h3>
        <hr>
        <ul class="list-unstyled text-center">
            <li>
                <img class="icon" src="img/general/home.png" alt="Home"/>
                <p class="icon">Subotička 67 21 000 Novi Sad, Srbija</p>
            </li>
            <li>
                <img class="icon" src="img/general/phone.png" alt="Phone"/>
                <p class="icon">fax 021/ 2466-467 mob 063/8-063-531 mob 064/11-59-532</p>
            </li>
            <li>
                <img class="icon" src="img/general/email.png" alt="Email"/>
                <p class="icon">rololuxns@yahoo.com</p>
            </li>
        </ul>

        <h3 class="album_h2">Kako nas pronaći</h3>
        <hr>
        <div class='embed-container maps'>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2809.348516435344!2d19.8143808095757!3d45.240744796632924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b102fbcfe803f%3A0xebe83980b39f34ad!2z0KHRg9Cx0L7RgtC40YfQutCwIDY3LCDQndC-0LLQuCDQodCw0LQgMjExMjQ!5e0!3m2!1ssr!2srs!4v1498039268694" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </section>

    <div class="clearfix"></div>
</main>

<?php include 'footer.php'; ?>
</body>
</html>
