<!DOCTYPE html>
<html lang="en">

<?php include 'head.php';?>

<body>
<header>
    <?php include 'navigation.php'; ?>
</header>

<main>
    <section class="container">
        <div id="carouselControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselControls" data-slide-to="1"></li>
                <li data-target="#carouselControls" data-slide-to="2"></li>
                <li data-target="#carouselControls" data-slide-to="3"></li>
                <li data-target="#carouselControls" data-slide-to="4"></li>
                <li data-target="#carouselControls" data-slide-to="5"></li>
                <li data-target="#carouselControls" data-slide-to="6"></li>
                <li data-target="#carouselControls" data-slide-to="7"></li>
                <li data-target="#carouselControls" data-slide-to="8"></li>
                <li data-target="#carouselControls" data-slide-to="9"></li>
                <li data-target="#carouselControls" data-slide-to="10"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="img-box carousel-item active justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese1"></div>
                    <!--                    <img class="img-size d-block img-fluid img-responsive img-center" src="/img/slider/pic1.jpg" alt="Roletne">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic2.jpg" alt="Tende">-->
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese2"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese3"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic3.jpg" alt="Trakaste zavese">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese4"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese5"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese6"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese7"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese8"d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese9"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese10"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_trakastezavese11"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <section class="container">
        <div class="row">
            <div class="col-lg-8 section-text">
                <h2>Trakaste zavese</h2>
                <p>Nudimo vam svoj proizvodni program trakastih zavesa koje su se pokazale kao vrlo praktično sredstvo za zaštitu od sunca i neželjenih pogleda, a u isto vreme Vaš prostor dobija novu estetsku dimenziju. Izrađene su od impregniranog platna u širokoj paleti boja, lako se održavaju, a mogu se zaokretati za 180 stepeni. Skrivaju od pogleda znatiželjnih lica i kod laganog zaokreta, a kada se potpuno zaokrenu, u Vaš prostor jos uvek dolazi dovoljno svetla.</p>
            </div>
            <?php include 'aside_najtrazenije.php';?>
        </div>
    </section>
</main>

<?php include 'footer.php'; ?>
</body>
</html>