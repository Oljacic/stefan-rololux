<?php

$uri = $_SERVER['REQUEST_URI'];

switch ($uri)
{
    case '/':
        include 'home.php';
        break;

    case '/roletne':
        include 'roletne.php';
        break;

    case '/tende':
        include 'tende.php';
        break;

    case '/venecijaneri':
        include 'venecijaneri.php';
        break;

    case '/zavese-rolo':
        include 'rolo_zavese.php';
        break;

    case '/zavese-trakaste':
        include 'trakaste_zavese.php';
        break;

    case '/komarnici':
        include 'komarnici.php';
        break;

    case '/dihtovanje':
        include 'dihtovanje.php';
        break;

    case '/servis':
        include 'servis.php';
        break;

    case '/adaptacija':
        require 'adaptacija.php';
        break;
    
    default:
        require '404.php';
}

?>