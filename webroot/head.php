<head>
    <meta charset="utf-8">
    <title>RoloLux <?php echo $uri ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Izrada, popravka i održavanje Aluminijumskih i PVC roletni, Tendi, Venecijanera, Trakastih zavesa, Rolo komarnika, Plisiranih zavesa, Harmonika vrata, Dihtovanje prozora i vrata aluminijumskim lajsnama">
    <meta name="keywords" content="izrada, popravke, održavanje, roletne, tende, venecijaneri, zavesa, komarnik, dihtovanje, prozor, vrata ">
    <link rel="icon" type="image/png" href="/img/faviconRL.png" />
    <link rel="stylesheet" href="/css/styles.css">
</head>

