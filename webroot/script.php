<script src="/js/jquery.min.js"></script>
<script src="/js/tether.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/rololux.js"></script>

<!-- Script js -->

<script>
    $(document).ready(function() {
        $('.maps').click(function () {
            $('.maps iframe').css("pointer-events", "auto");
        });

        $( ".maps" ).mouseleave(function() {
            $('.maps iframe').css("pointer-events", "none");
        });
    });
</script>
