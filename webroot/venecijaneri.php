<!DOCTYPE html>
<html lang="sr-RS">

<?php include 'head.php';?>

<body>
<header>
    <?php include 'navigation.php'; ?>
</header>

<main>
    <section class="container">
        <div id="carouselControls" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselControls" data-slide-to="0" class="active"></li>
                <li data-target="#carouselControls" data-slide-to="1"></li>
                <li data-target="#carouselControls" data-slide-to="2"></li>
                <li data-target="#carouselControls" data-slide-to="3"></li>
                <li data-target="#carouselControls" data-slide-to="4"></li>
                <li data-target="#carouselControls" data-slide-to="5"></li>
                <li data-target="#carouselControls" data-slide-to="6"></li>
                <li data-target="#carouselControls" data-slide-to="7"></li>
                <li data-target="#carouselControls" data-slide-to="8"></li>
                <li data-target="#carouselControls" data-slide-to="9"></li>
                <li data-target="#carouselControls" data-slide-to="10"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="img-box carousel-item active justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri1"></div>
                    <!--                    <img class="img-size d-block img-fluid img-responsive img-center" src="/img/slider/pic1.jpg" alt="Roletne">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic2.jpg" alt="Tende">-->
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri2"></div>
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri3"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic3.jpg" alt="Trakaste zavese">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri4"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri5"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri6"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri7"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri8"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri9"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri10"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
                <div class="img-size img-box carousel-item justify-content-center">
                    <div class="d-block img-fluid img-responsive img-center slider" id="slider_venecijaneri11"></div>
                    <!--                    <img class="d-block img-fluid img-responsive img-center" src="/img/slider/pic4.jpg" alt="Panel">-->
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <section class="container">
        <div class="row">
            <div class="col-lg-8 section-text">
                <h2>Venecijaneri</h2>
                <p>Popularna venecijaner zavesa od legiranog aluminijuma se lako održava Odaberite boju i dezen koji se uklapa sa nameštajem i bojom zidova Venecijaneri se ugrađuju sa unutrašnje strane prozora uz samo staklo ali nepostoji mogućnost njegovog oštećenja.</p>
                <p>Drvena venecijaner zavesa stvara intimnu atmosferu i predstavlja ukras visoke estetetske vrednosti.</p>
                <p>Aluminijumski venecijaneri se izrađuju u velikom spektru boja i dezena. Kupac može da bira između tekstura mramora, granita, drveta i drugih prirodnih materijala. Posebno zanimljivu igru svetlosti i senke stvaraju venecijaneri sa perforiranom površinom.</p>
                <p>Trake venecijanera se pomeraju, tj. otvaraju i zatvaraju pomoću plastične palice Pomeranjem palice okrećemo trake i određujemo količinu svetlosti koju puštamo u prostoriju. Venecijaneri se mogu podizati i spuštati.</p>
                <p>Održavanje venecijanera je veoma lako i jednostavno Peru se identično kao i prozori.</p>
            </div>
            <?php include 'aside_najtrazenije.php';?>
        </div>
    </section>
</main>

<?php include 'footer.php'; ?>
</body>
</html>
